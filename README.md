Sadhana planner app
=====================

Sadhana is Ionic based mobile app.

## Environment setup

* Install Node. See: [ https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager ]( https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager ).

* Install [Ionic CLI](https://github.com/driftyco/ionic-cli):

```bash
npm install -g ionic
```

* Install all project dependencies 

```bash
npm install
```

* Build & serve project locally
```
npm start
```

[http://localhost:3000/]( http://localhost:3000/ ) will open automatically in your browser

## Development

Use:

```
gulp
```

task for development. It will build current project version, run  server and reload on changes. Dev version is build to `out` folder.

## Build

All cordova plugins are listed in `config.xml` file and should be installed automatically. In case of any issues you can install it manually.

```
cordova plugin add cordova-plugin-whitelist
cordova plugin add cordova-plugin-inappbrowser
cordova plugin add cordova-plugin-camera
cordova plugin add cordova-plugin-crosswalk-webview
cordova plugin add https://github.com/whiteoctober/cordova-plugin-app-version.git
cordova plugin add phonegap-plugin-push --variable SENDER_ID="182605508841"
cordova plugin add cordova-plugin-media-capture
```

Use

```gulp build``` 

task for building project dist version to `www` folder.

#Technology stack

* AngularJs 1.5.3
* ES6 ( using Babel Compiler )
* Sass styles
* Gulp for building process


## Test users credentials:

Coach

```
username: "testuser12@mailedu.com"
password: "testPwd"
```

Subscriber free

```
username: "testuser123@mailedu.com"
password: "testPwd"
```

## Convention and style guide

* indent using spaces ( 2 spaces per tab )
* Angular code following [John Papa style guide](https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md)
* Code formatting style consistent with `/.jscsrc` configuration file. Please use [jscs](http://jscs.info/) plugin with your IDE.
* Code linting consistent with `/.jshintrc` configuration file. Please use [jshint](http://jshint.com/) plugin with your IDE.
* Project structure
    * All gulp files are placed in `gulp` folder
    * All app files are placed in `src` folder
    * Place shared components inside `src/app/components` folder
    * Place views inside `src/app/views` folder nested in the same way it is defined in the app
    * Use component based structure - place view controller, template and styles in the same folder named same as component
    * For nested views use parent prefix for file name
    * Place global styles inside `src/app/styles`
* File name convention
    * For JS files add component type suffix. Eg:
        * `module` suffix for module definition `test.module.js` 
        * `service` suffix for module definition `test.service.js` 
        * `controller` suffix for module definition `test.controller.js`
    * For html templates and styles use same name as component
