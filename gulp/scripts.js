'use strict';

var path = require('path');
var gulp = require('gulp');
var babel = require('gulp-babel');
var conf = require('./conf');

gulp.task('scripts', function () {
  return jsStream({ jsPaths: 'src/app/**/*.js', dir: path.join(conf.paths.tmp, '/serve/app'), base: 'src/app' });
});

gulp.task('scripts:watch', ['scripts'], function () {
  return gulp.watch('src/app/**/*.js', ['scripts']);
});

function jsStream(config) {
  return gulp.src(config.jsPaths, { base: config.base })
      .pipe(babel({
        presets: ['es2015']
      }))
      .pipe(gulp.dest(config.dir));
}
