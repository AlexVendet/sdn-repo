angular
.module('app')
.config(function ($urlRouterProvider) {

  redirectInvalidUrlsTo('/');

  // ignoreTrailingSlashWhenMatchingUrlToState();

  redirectRootUrlTo(['activeSession', 'UserRoleService', function (activeSession, UserRoleService) {
    let sessionInfo = activeSession.sessionInfo();
    if (!activeSession.isPresent()) {
      return '/signUp';
    } else if (sessionInfo.user && UserRoleService.isSubscriber()) {
      console.log('redirecting!!! ');
      return '/menu/programs?tab=join';
    } else {
      return '/menu/organization//programs?tab=upload';
    }
  }]);

  function redirectInvalidUrlsTo(path) {
    $urlRouterProvider.otherwise(path);
  }

  function redirectRootUrlTo(path) {
    $urlRouterProvider.when('', path);
    $urlRouterProvider.when('/', path);
  }

})

.run(redirectNotAuthenticatedUserToSignInPage)
.run(showLoadingScreenOnRouteChange)
.run(redirectOnRouteChangeError);

function redirectNotAuthenticatedUserToSignInPage($log, $rootScope, $state, activeSession, $location) {
  $rootScope.$on('$stateChangeStart', function (event, toState) {
    if (hasPrivateAccess(toState) && !activeSession.isPresent()) {
      $log.log('Desired page is private and you are not authenticated. You will be redirected to Sign In page.');
      stopLoadingDesiredPage(event);
      $state.go('signUp');
    } else if (toState.name === 'signUp' && activeSession.isPresent()) {
      stopLoadingDesiredPage(event);
      $location.path('/');
    }
  });

  function hasPrivateAccess(state) {
    return angular.isUndefined(state.data) || (state.data.hasPublicAccess !== true);
  }

  function stopLoadingDesiredPage(event) {
    event.preventDefault();
  }
}

function showLoadingScreenOnRouteChange($rootScope, $ionicLoading) {
  $rootScope.$on('$stateChangeStart', (event) => {
    // do not show loading if stopLoadingDesiredPage() was fired before
    if (!event.defaultPrevented) {
      $ionicLoading.show();
    }
  });

  $rootScope.$on('$stateChangeSuccess', () => {
    $ionicLoading.hide();
  });

  $rootScope.$on('$stateChangeError', () => {
    $ionicLoading.hide();
  });
}

function redirectOnRouteChangeError($rootScope, $state) {
  $rootScope.$on('$stateChangeError', () => {
    $state.go('signUp');
  });
}
