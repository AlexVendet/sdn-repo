/* globals PushNotification */
'use strict';

angular.module('app', [
  // vendors
  'ionic',
  'ui.router',
  'ngCordova',
  'angularMoment',
  'ngTagsInput',

  'app.services',
  'app.directives',
  'app.filters',
  'app.components',
  'app.constants',
  'app.partials',
  'app.session',
  'app.http',
  'app.userProfile',
  'app.signUp',
  'app.organizations',
  'app.programs',
  'app.program.items',
  'app.program.item.resources',
  'app.helpAndFeedback',
  'app.notifications'
  ]).run(function ($ionicPlatform, AuthenticationService, UserService, DeviceService, NotificationCallbackService) {
    $ionicPlatform.ready(() => {
      if (window.PushNotification && typeof window.PushNotification.init === 'function') {
        var push = PushNotification.init({
          android: {
            senderID: '182605508841'
          },
          browser: {
            pushServiceURL: 'http://push.api.phonegap.com/v1/push'
          },
          ios: {
            alert: 'true',
            badge: 'true',
            sound: 'true'
          },
          windows: {}
        });

        PushNotification.hasPermission(data => {
          if (data.isEnabled) {
            console.log('Push Notifications are enabled');
          } else {
            console.log('Push Notifications are disabled');
          }
        });

        push.on('registration', function (data) {
          console.log('DEBUG: cordovaPushV5 registration' + JSON.stringify(data));
          console.log('typeof push.finish: ', typeof push.finish === 'function');
          AuthenticationService.onSignIn(({ user }) => {
            DeviceService.getAll()
            .then(devices => {
              console.log('devices! ', devices);
              let addedDevice = _.find(devices, { deviceToken: data.registrationId });
              if (data.registrationId && !addedDevice) {
                let device = {
                  deviceToken: data.registrationId,
                  timeZone: 'GMT' + moment().format('Z').replace('+0', '+'),
                  userId: user.id,
                  deviceType: DeviceService.getDeviceServiceType()
                };
                console.log('not find! device! ', device);

                DeviceService.create(device)
                .then(response => {
                  console.log('device added response ', response);
                  removeDeviceOnSignOut(response.data.id);
                });
              } else {
                console.log('device already added');
                removeDeviceOnSignOut(addedDevice.id);
              }
            });
          });
        });

        push.on('notification', function (data) {
          console.log('notification ' + JSON.stringify(data));
          NotificationCallbackService.handle(data.additionalData);
          if (typeof push.finish === 'function') {
            push.finish(() => {
              console.log('processing of push data is finished');
            }, () => {
              console.error('something went wrong with push.finish for ID = ' + data.additionalData.notId);
            }, data.additionalData.notId);
          }
        });

        push.on('error', function (error) {
          console.log('error ' + JSON.stringify(error));
          window.alert('Notification error message: ' + JSON.stringify(error));
        });
      } else {
        console.log('No window.PushNotification API avaliable: ', window.PushNotification);
      }

      function removeDeviceOnSignOut(deviceId) {
        return AuthenticationService.onSignOut(({ user }) => {
          console.log('Removing device id before user logout ...');
          return DeviceService.remove(deviceId)
            .then(() => {
              console.log('Device successfully removed');
            })
            .catch((err) => {
              console.error('error ', err);
            });
        });
      }
    });
  });
