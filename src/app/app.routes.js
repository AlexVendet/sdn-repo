angular
.module('app')
.config(function ($stateProvider) {

  $stateProvider

  .state('signUp', {
    url: '/signUp',
    templateUrl: 'views/sign-up/sign-up.html',
    controller: 'SignUpController',
    data: {
      hasPublicAccess: true
    }
  })
  .state('menu', {
    url: '/menu',
    abstract: true,
    templateUrl: 'components/menu/menu.html',
    controller: 'MenuController'
  })
  .state('menu.userProfile', {
    url: '/userProfile?welcome',
    params: {
      welcome: {
        value: 'false',
        squash: true
      }
    },
    templateUrl: 'views/user/profile/user-profile.html',
    controller: 'UserProfileController',
    resolve: {
      userData: ['UserService', UserService => UserService.get()]
    }
  })
  .state('menu.tabs', {
    url: '/tabs',
    templateUrl: 'templates/coach_active_add_videoItem.html',
    controller: 'tabsCtrl'
  });

});
