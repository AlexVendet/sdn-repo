angular.module('app.components')
.component('backButton', {
  templateUrl: 'components/back-button/back-button.html',
  controller: 'BackButtonController'
})

.controller('BackButtonController', function ($ionicHistory) {
  this.goBack = () => {
    $ionicHistory.goBack();
  };
});
