angular
.module('app.components')
.directive('iconClass', function() {
  return {
    restrict: 'A',
    scope: {
      itemType: '='
    },
    link: function(scope, element, attr, controller) {
      const itemIcons = {
        'PHRASE': 'ion-ios-paper-outline',
        'IMAGE': 'ion-image',
        'VIDEO': 'ion-ios-videocam',
        'AUDIO': 'ion-volume-medium',
        'CHECKLIST': 'ion-ios-list-outline',
        'DND': 'ion-android-volume-off'
      };

      let setClass = () => {
        for(let [key, iconClass] of Object.entries(itemIcons)) {
          element.removeClass(iconClass);
        }

        let iconClass = itemIcons[scope.itemType];
        if(iconClass) {
          element.addClass(iconClass);
        } else {
          console.error(`iconClass is undefined. There is no ${scope.itemType} type specified in itemIcons object.`);
        }
      };

      scope.$watch('itemType', setClass, true);
    }
  };
});
