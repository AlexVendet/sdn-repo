angular.module('app.components')
.controller('MenuController', function ($scope, $state, AuthenticationService, $ionicLoading, $ionicSideMenuDelegate, UserRoleService, $location) {
  $scope.profile = function () {
    $state.go('settings');
  };

  $scope.logout = () => {
    $ionicLoading.show();
    return AuthenticationService.signOut()
      .finally(() => {
        $state.go('signUp');
      });
  };

  $scope.toggleLeft = () => {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.isCoach = UserRoleService.isCoach;
  $scope.isSwitchedToSubscriber = UserRoleService.isSwitchedToSubscriber;

  $scope.switchRole = () => {
    if (UserRoleService.isSwitchedToSubscriber()) {
      UserRoleService.switchToCoach();
    } else {
      UserRoleService.switchToSubscriber();
    }
    $location.path('/');
  };
});
