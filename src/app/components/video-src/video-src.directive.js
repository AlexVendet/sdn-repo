angular.module('app.components')
.directive('videoSrc', function () {
  return {
    restrict: 'A',
    scope: {
      videoSrc: '='
    },
    link: function postLink(scope, element, attr) {
      let setVideo = val => {
        element.children().attr('src', val);
      };

      setVideo(scope.videoSrc);
      scope.$watch('videoSrc', setVideo);
    }
  };
});
