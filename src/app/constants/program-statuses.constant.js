angular
.module('app.constants')
.constant('programStatus', {
  PUBLISHED: 'PUBLISHED',
  DRAFT: 'DRAFT',
  FROZEN: 'FROZEN',
  DELETED: 'DELETED',
  UNPUBLISHED: 'UNPUBLISHED',
  PAUSED: 'PAUSED'
});
