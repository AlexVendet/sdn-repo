/* global ionic */

angular.module('app.directives')
.directive('browseTo', function ($ionicGesture) {
  return {
    restrict: 'A',
    link: function ($scope, $element, $attrs) {
      console.log('browse to directive!');
      var handleTap = function (event) {
        window.open(encodeURI($attrs.browseTo), '_system');
      };
      var tapGesture = $ionicGesture.on('tap', handleTap, $element);
      $scope.$on('$destroy', function () {
        $ionicGesture.off(tapGesture, 'tap', handleTap);
      });
    }
  };
});
