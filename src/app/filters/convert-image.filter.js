'use strict';

angular.module('app.filters')
.filter('convertImage', convertImage);

function convertImage(UploaderService) {
  const PROCESS_BASE_URL = 'https://process.filestackapi.com/';
  const DEFAULT_OPTIONS = { resize: { width: 100 } };
  return function (url, options=DEFAULT_OPTIONS) {
    if (!url || !url.startsWith('http')) {
      return url;
    }
    let parsedOptions = parseOptions(options);
    if (parsedOptions) {
      parsedOptions = '/' + parsedOptions + '/';
    }
    return PROCESS_BASE_URL + UploaderService.FILESTACK_PUBLIC_KEY + parsedOptions + url;
  };
}

function parseOptions(obj={}) {
  return _.map(obj, (value, key) => {
    if (typeof value === 'object') {
      return key + '=' + _.map(value, (itemValue, itemKey) => {
        return itemKey + ':' + String(itemValue);
      }).join(',');
    } else {
      return key + '=' + 'value';
    }
  }).join('&');
}
