
angular.module('app.filters')
.filter('limitWordsTo', limitWordsTo);

function limitWordsTo() {
  return (string='', numberOfWords=15) => {
    let arrayOfWords = string.split(' ');
    let limit = numberOfWords > arrayOfWords.length ? arrayOfWords.length : numberOfWords;
    let ending = limit === numberOfWords ? '...' : '';

    return arrayOfWords
            .slice(0, limit)
            .join(' ')
            .concat(ending);
  };
}
