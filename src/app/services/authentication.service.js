angular.module('app.services')
.service('AuthenticationService', function ($http, ServerUrls, UserService, activeSession, $q) {

  const AUTHORIZATION_KEY = 'Basic dXNlcjpRV0VSVFk=';
  let onSignInArr = [];
  let onSignOutArr = [];

  return {
    signIn,
    signOut,
    refresh,
    onSignIn,
    onSignOut
  };

  function signIn({ username, password }) {
    var headers = {
      Authorization: AUTHORIZATION_KEY,
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    return $http.post(ServerUrls.LOGIN, null, {
        params: {
          username,
          password: password,
          grant_type: 'password'
        },
        headers
      })
      .then(response => {
        activeSession.setTo(response.data);
        return setUserInfo()
          .then(() => {
            onSignInArr.forEach(fn => fn(activeSession.sessionInfo()));
            return response;
          });
      });
  }

  function refresh() {
    var headers = {
      Authorization: AUTHORIZATION_KEY,
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    return $http.post(ServerUrls.LOGIN, null, {
        params: {
          refresh_token: activeSession.sessionInfo().refresh_token,
          grant_type: 'refresh_token'
        },
        headers
      })
      .then(response => {
        console.log('============================ REFRESH TOKEN ============================');
        console.log('response: ', response);
        console.log('============================ REFRESH TOKEN ============================');
        activeSession.setTo(response.data);
        return setUserInfo()
          .then(() => response);
      })
      .catch(error => {
        activeSession.clear();
        return error;
      });
  }

  function signOut() {
    var headers = {
      Authorization: AUTHORIZATION_KEY,
      'Content-Type': 'application/x-www-form-urlencoded'
    };

    return $http.post(ServerUrls.LOGOUT, null, {
        params: {
          token: activeSession.sessionInfo().access_token
        },
        headers
      })
      .finally(() => {
        return $q.all(onSignOutArr.map(fn => fn(activeSession.sessionInfo())))
          .finally(() => {
            activeSession.clear();
          });
      });
  }

  function setUserInfo() {
    return UserService.get()
      .then(userResponse => {
        activeSession.setUserInfo(userResponse);
        return userResponse;
      });
  }

  function onSignIn(fn) {
    if (fn && typeof fn === 'function') {
      let index = onSignInArr.indexOf(fn);
      if (index > -1) {
        onSignInArr.splice(index, 1);
      } else {
        onSignInArr.push(fn);
        console.log('is ? ', activeSession.isPresent());
        if (activeSession.isPresent()) {
          fn(activeSession.sessionInfo());
        }
      }
    }
  }

  function onSignOut(fn) {
    if (fn && typeof fn === 'function') {
      let index = onSignOutArr.indexOf(fn);
      if (index > -1) {
        onSignOutArr.splice(index, 1);
      } else {
        onSignOutArr.push(fn);
      }
    }
  }
});
