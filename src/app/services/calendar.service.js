angular
.module('app.services')
.factory('calendar', calendar);

function calendar(moment) {
  const formats = {
    DEFAULT: 'YYYY-MM-DD',
    USER: 'D MMM YYYY'
  };

  const weekDays =  [
    { value: 'MONDAY', name: 'M' },
    { value: 'TUESDAY', name: 'T' },
    { value: 'WEDNESDAY', name: 'W' },
    { value: 'THURSDAY', name: 'T' },
    { value: 'FRIDAY', name: 'F' },
    { value: 'SATURDAY', name: 'S' },
    { value: 'SUNDAY', name: 'S' }
  ];

  return {
    getWeek,
    formats,
    timeToDate,
    weekDays
  };

  function getWeek(day=moment().format(formats.DEFAULT)) {
    return _.range(1, 8).map(num =>
      moment(day).isoWeekday(num)
    );
  }

  function timeToDate(time='') {
    let [hour, minute] = time.split(':');
    return new Date(moment().set({ hour: hour || 0, minute: minute || 0, second: 0, millisecond: 0 }));
  }
}
