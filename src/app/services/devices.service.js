/* global ionic */

angular.module('app.services')
.service('DeviceService', function ($http, ServerUrls) {
  const DEVICE_BASE_URL = ServerUrls.API_BASE + '/user/self/device';

  return {
    create,
    update,
    remove,
    get,
    getAll,
    getDeviceServiceType
  };

  function create(data) {
    return $http.post(DEVICE_BASE_URL, data);
  }

  function update({ id, data }) {
    return $http.put(DEVICE_BASE_URL + '/' + id, data);
  }

  function get(id) {
    return $http.get(DEVICE_BASE_URL + '/' + id)
      .then(response => {
        return response.data;
      });
  }

  function remove(id) {
    return $http.delete(DEVICE_BASE_URL + '/' + id)
      .then(response => {
        return response.data;
      });
  }

  function getAll() {
    return $http.get(DEVICE_BASE_URL + 's')
      .then(response => response.data);
  }

  function getDeviceServiceType() {
    if (!ionic || !ionic.Platform) {
      return 'GCM';
    }
    if (ionic.Platform.isIOS()) {
      return 'APNS';
    } else if (ionic.Platform.isAndroid()) {
      return 'GCM';
    } else if (ionic.Platform.isWindowsPhone()) {
      return 'WNS';
    } else {
      return 'GCM';
    }
  }
});
