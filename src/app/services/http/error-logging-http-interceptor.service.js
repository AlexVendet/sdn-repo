angular
    .module('app.http')
    .factory('errorLoggingHttpInterceptor', function ($q, $log) {
        return {
            responseError: function (response) {
                $log.error(response);
                return $q.reject(response);
              }
          };
      });
