(function () {
  'use strict';

  angular
    .module('app.http')
    .factory('httpStatus', httpStatus);

  function httpStatus() {
    return {
      _0_ERR_CONNECTION_REFUSED: 0,
      _200_OK: 200,
      _401_UNAUTHORIZED: 401,
      _409_CONFLICT: 409,
      _500_INTERNAL_SERVER_ERROR: 500
    };
  }

})();
