angular.module('app.services')
.service('ServerUrls', function () {
  const SERVER_URL = 'http://sdn-preapi.eu-west-1.elasticbeanstalk.com';
  const API_BASE = `${SERVER_URL}/api`;

  return {
    API_BASE,
    USER_CREATE: `${API_BASE}/user/create`,
    USER_UPDATE: `${API_BASE}/user/update`,
    USER_GET: `${API_BASE}/user/self`,
    LOGIN: `${SERVER_URL}/oauth/token`,
    LOGOUT: `${SERVER_URL}/oauth/revoke`
  };
});
