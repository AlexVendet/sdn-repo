/* globals filepicker */
angular.module('app.services')
.service('UploaderService', function ($q, ServerUrls) {

  const FILESTACK_PUBLIC_KEY = 'AKmtT4VpQJuzkx1pM7AiQz';
  const S3_BUCKET = 'sdn-depot';
  const S3_BASE_URL = 'https://s3-eu-west-1.amazonaws.com';
  filepicker.setKey(FILESTACK_PUBLIC_KEY);

  let DEFAULT_OPTIONS = {
    services: ['COMPUTER', 'CONVERT', 'WEBCAM'],
    multiple: false,
    mimetypes: ['image/*'],
    container: 'modal'
  };

  const DEFAULT_STORE_OPTIONS = {
    location: 'S3',
    access: 'public',
    bucket: S3_BUCKET
  };

  return {
    uploadDialog,
    upload,
    buildAwsPath,
    remove,
    FILESTACK_PUBLIC_KEY
  };

  function buildAmazonUrl({ key, bucket=S3_BUCKET }) {
    return `${S3_BASE_URL}/${bucket}/${key}`;
  }

  function buildAwsPath({ organizationId, userId, programId, itemId, customFolderName, customFilename }) {
    let path = '/';
    path += angular.isDefined(organizationId) ? `organizations/${organizationId}` : `users/${userId}`;
    path += angular.isDefined(programId) ? `/programs/${programId}` : '';
    path += angular.isDefined(itemId) ? `/items/${itemId}` : '';
    path += angular.isDefined(customFolderName) ? `/${customFolderName}` : '';
    path += angular.isDefined(customFilename) ? `/${customFilename}` : '/';
    return path;
  }

  function uploadDialog({ options = {}, storeOptions = {}, pathOptions } = {}) {
    let defer = $q.defer();
    options = _.defaults(options, DEFAULT_OPTIONS);
    storeOptions = _.defaults(storeOptions, DEFAULT_STORE_OPTIONS);
    if (_.isObject(pathOptions)) {
      storeOptions.path = buildAwsPath(pathOptions);
    }
    filepicker.pickAndStore(options, storeOptions, onSuccess, onError, onProgress);
    return defer.promise;

    function onSuccess(data) {
      data.forEach(item => {
        item.amazonUrl = buildAmazonUrl({ key: item.key });
      });
      defer.resolve(data);
    }

    function onError(error) {
      defer.reject(error);
    }

    function onProgress(progress) {
      defer.notify(progress);
    }
  }

  function upload({ value, storeOptions = {}, pathOptions } = {}) {
    let defer = $q.defer();
    storeOptions = _.defaults(storeOptions, DEFAULT_STORE_OPTIONS);
    if (_.isObject(pathOptions)) {
      storeOptions.path = buildAwsPath(pathOptions);
    }
    filepicker.store(value, storeOptions, onSuccess, onError, onProgress);
    return defer.promise;

    function onSuccess(item) {
      item.amazonUrl = buildAmazonUrl({ key: item.key });
      defer.resolve([item]);
    }

    function onError(error) {
      defer.reject(error);
    }

    function onProgress(progress) {
      defer.notify(progress);
    }
  }

  function remove(url) {
    let defer = $q.defer();
    filepicker.remove({ url }, {}, onSuccess, onError);
    return defer.promise;

    function onSuccess(response) {
      defer.resolve(response);
    }

    function onError(error) {
      defer.reject(error);
    }
  }
});
