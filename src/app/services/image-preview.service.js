/* global ionic */

angular.module('app.services')
.service('ImagePreviewService', function ($ionicModal, $rootScope) {
  return {
    open
  };

  function open(url) {
    let scope = $rootScope.$new();
    scope.url = url;

    scope.closeModal = () => {
      if (scope.modal && typeof scope.modal.remove === 'function') {
        scope.modal.remove();
      }
    };

    return $ionicModal.fromTemplateUrl('services/image-preview.modal.html', {
      scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      console.log('then modal ', modal);
      scope.modal = modal;
      modal.show();
      return modal;
    });
  }
});
