angular
.module('app.services')
.factory('NotificationCallbackService', NotificationCallbackService);

function NotificationCallbackService($state, $q) {
  return {
    handle
  };

  function handle(data) {
    console.log('handle data! ', data);
    if (!_.isUndefined(data.resource_type) && !_.isUndefined(data.programItemResourceId)) {
      return showContent(data);
    } else if (!_.isUndefined(data.programItemId) && !_.isUndefined(data.publishDate)) {
      return uploadContent(data);
    } else {
      return $q.reject();
    }
  }

  function showContent(data) {
    return $state.go('menu.program.resource.' + _.toLower(data.resource_type), {
        programId: data.programId,
        itemId: data.programItemId,
        resourceId: data.programItemResourceId
      });
  }

  function uploadContent(data) {
    return $state.go('menu.program.item.resource.edit.' + _.toLower(data.resource_type), {
        programId: data.programId,
        itemId: data.programItemId,
        resourceId: data.programItemResourceId,
        publishDate: data.publishDate
      });
  }
}
