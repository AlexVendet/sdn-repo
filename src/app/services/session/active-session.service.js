angular
.module('app.session')
.service('activeSession', function () {

  let sessionCookie = 'activeSession';
  let storedSessionInfo = sessionInfo();

  return {
    isPresent,
    sessionInfo,
    setUserInfo,
    setTo,
    clear
  };

  function isPresent() {
    return isValid(sessionInfo());
  }

  function sessionInfo() {
    var sessionInfoFromCookie;
    try {
      sessionInfoFromCookie = JSON.parse(localStorage.getItem(sessionCookie));
    } catch (error) {
      sessionInfoFromCookie = null;
    }
    if (isValid(sessionInfoFromCookie)) {
      return sessionInfoFromCookie;
    }
    if (isValid(storedSessionInfo)) {
      return storedSessionInfo;
    }
    clear();
    return storedSessionInfo;
  }

  function setUserInfo(user) {
    let currentSessionInfo = sessionInfo();
    currentSessionInfo.user = user;
    setTo(currentSessionInfo);
  }

  function isValid(sessionInfo) {
    return !!sessionInfo && !!sessionInfo.access_token;
  }

  function setTo(sessionInfo) {
    localStorage.setItem(sessionCookie, JSON.stringify(sessionInfo));
    storedSessionInfo = sessionInfo;
  }

  function clear() {
    localStorage.removeItem(sessionCookie);
    storedSessionInfo = undefined;
  }

});
