angular
    .module('app.session')
    .factory('authenticationHttpInterceptor', authenticationHttpInterceptor);

function authenticationHttpInterceptor($q, $log, $injector, ServerUrls, httpStatus, activeSession) {

  let authorizationHeaderKey = 'Authorization';
  let serverMatcher = new RegExp(_.escapeRegExp(ServerUrls.API_BASE) + '.*');
  let inflightAuthRequest;

  return {
    request: config => {
      let url = config.url;
      if (!isUrlOfServer(url)) {
        return config;
      }
      let headers = config.headers;
      if (doesContainAuthorizationHeader(headers)) {
        return config;
      }
      if (!activeSession.isPresent()) {
        return config;
      }
      headers[authorizationHeaderKey] = 'Bearer ' + activeSession.sessionInfo().access_token;
      return config;
    },

    responseError: response => {
      switch (response.status) {
        case httpStatus._401_UNAUTHORIZED:
          if (activeSession.isPresent() && activeSession.sessionInfo().refresh_token) {
            inflightAuthRequest = inflightAuthRequest || $injector.get('AuthenticationService').refresh();
            inflightAuthRequest.then(
              () => retry(removeAuthorizationHeader(response.config)),
              () => $q.reject(response))
            .finally(() => {
              inflightAuthRequest = null;
            });
          } else {
            $log.log('You are not authenticated. Session will be cleared. You will be redirected to Sign In page.');
            activeSession.clear();
            goToSignInPage();
          }
          break;
      }
      return $q.reject(response);
    }

  };

  function retry(config) {
    return $injector.get('$http')(config);
  }

  function isUrlOfServer(url) {
    return serverMatcher.test(url);
  }

  function doesContainAuthorizationHeader(headers) {
    return _.has(headers, authorizationHeaderKey);
  }

  function goToSignInPage() {
    $injector.get('$state').go('signUp');
  }

  function removeAuthorizationHeader(config) {
    console.log('removeAuthorizationHeader config: ', config, config.headers, config.headers[authorizationHeaderKey]);
    delete config.headers[authorizationHeaderKey];
    return config;
  }

}
