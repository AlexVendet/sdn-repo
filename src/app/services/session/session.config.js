angular
  .module('app.session')
  .config(function ($httpProvider) {
    $httpProvider.interceptors.push('authenticationHttpInterceptor');
  });
