angular
    .module('app.session', [
        /* vendor */
        'ui.router',
        /* app */
        'app.http'
    ]);
