angular
.module('app.services')
.factory('stateHelper', stateHelper);

function stateHelper($state, $stateParams) {
  return {
    updateParams: updateParams,
    getStateParams: getStateParams,
    reload
  };

  function updateParams(newState) {
    var state = _.omit(_.assign($stateParams, newState), _.isUndefined);
    if (!_.isNull($state.transition)) {
      return;
    }
    $state.go($state.current, state, {
      notify: false,
      reload: true
    });
  }

  function reload() {
    return $state.transitionTo($state.current, $stateParams, {
      reload: true,
      inherit: false,
      notify: true
    });
  }

  function getStateParams() {
    return $stateParams;
  }
}
