angular
  .module('app.services')
  .service('SubscriptionsService', function ($http, ServerUrls) {
    const URL = ServerUrls.API_BASE + '/program';
    const URLS = {
      CREATE: URL + '/subscribe/',
      PAUSE: URL + '/subscribe/pause/',
      REMOVE: URL + '/subscribe/remove/'
    };

    return {
      create,
      pause,
      remove
    };

    function create({ id, timestamp = moment().format('x') }) {
      return $http.post(URLS.CREATE + `${id}/${timestamp}`);
    }

    function pause(id) {
      return $http.post(URLS.PAUSE + id);
    }

    function remove(id) {
      return $http.post(URLS.REMOVE + id);
    }
  });
