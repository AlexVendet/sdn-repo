angular.module('app.services')
.service('UserRoleService', function (activeSession) {
  const COACH_ROLES = ['COACH_PREMIUM', 'COACH'];
  let switchedTuSubscriber = false;

  return {
    isCoach,
    isSubscriber,
    switchToSubscriber,
    switchToCoach,
    isSwitchedToSubscriber
  };

  function isCoach() {
    return activeSession.isPresent() && _.includes(COACH_ROLES, (activeSession.sessionInfo().user || {}).role);
  }

  function isSubscriber() {
    return !isCoach() || switchedTuSubscriber;
  }

  function isSwitchedToSubscriber() {
    return switchedTuSubscriber;
  }

  function switchToSubscriber() {
    if (!isCoach()) {
      return;
    }
    switchedTuSubscriber = true;
  }

  function switchToCoach() {
    if (!isCoach()) {
      return;
    }
    switchedTuSubscriber = false;
  }
});
