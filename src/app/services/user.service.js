angular.module('app.services')
.service('UserService', function ($http, ServerUrls) {

  return {
    create,
    update,
    get
  };

  function create({ username, password }) {
    return $http.post(ServerUrls.USER_CREATE, {
      email: username,
      password
    });
  }

  function update({ id, data }) {
    return $http.post(ServerUrls.USER_UPDATE + '/' + id, map(data));
  }

  function get() {
    return $http.get(ServerUrls.USER_GET)
      .then(response => {
        if (response.data.dateOfBirth) {
          response.data.dateOfBirth = unixTimestampToDate(response.data.dateOfBirth);
        }
        return response.data;
      });
  }

  function map(data) {
    let submitFields = [
      // 'active',
      'cityCode',
      'countryCode',
      // 'email',
      'firstName',
      'gender',
      'dateOfBirth',
      'lastName',
      'phone',
      'devices'
      // 'role'
    ];
    let submitData = {};
    submitFields.forEach(field => {
      if (!_.isUndefined(data[field]) && !_.isNull(data[field])) {
        submitData[field] = data[field];
      }
    });

    return submitData;
  }

  function toUnixTimestamp(date) {
    return moment(date).unix();
  }

  function unixTimestampToDate(timestamp) {
    return new Date(Number(timestamp) * 1000);
  }
});
