angular
  .module('app.helpAndFeedback', [])
  .config(function ($stateProvider) {

    $stateProvider

    .state('menu.helpAndFeedback', {
      url: '/help-and-feedback',
      templateUrl: 'views/help-and-feedback/help-and-feedback.html',
      controller: 'HelpAndFeedbackController'
    })
    .state('menu.faq', {
      url: '/faq',
      templateUrl: 'views/help-and-feedback/faq/help-and-feedback-faq.html',
      controller: 'FaqController'
    })
    .state('menu.quickStart', {
      url: '/quick-start',
      templateUrl: 'views/help-and-feedback/quick-start/help-and-feedback-quick-start.html',
      controller: 'QuickStartController'
    })
    .state('menu.about', {
      url: '/about',
      templateUrl: 'views/help-and-feedback/about/help-and-feedback-about.html',
      controller: 'AboutController'
    })
    .state('menu.contact', {
      url: '/contact',
      templateUrl: 'views/help-and-feedback/contact/help-and-feedback-contact.html',
      controller: 'ContactController'
    })
    .state('menu.becomeCoach', {
      url: '/become-coach',
      templateUrl: 'views/help-and-feedback/become-coach/help-and-feedback-become-coach.html',
      controller: 'BecomeCoachController'
    })
    .state('menu.support', {
      url: '/support',
      templateUrl: 'views/help-and-feedback/support/help-and-feedback-support.html',
      controller: 'SupportController'
    })
    .state('menu.termsAndConditions', {
      url: '/terms-and-conditions',
      templateUrl: 'views/help-and-feedback/terms-and-conditions/help-and-feedback-terms-and-conditions.html',
      controller: 'TermsAndConditionsController'
    });

  });
