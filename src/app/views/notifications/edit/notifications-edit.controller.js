angular
.module('app.notifications')
.controller('NotificationsEditController', function ($scope, notification, $ionicPopup, NotificationsService, $ionicLoading, $state, calendar, item, $stateParams) {
  $scope.item = _.clone(item);
  $scope.item.startTime = calendar.timeToDate($scope.item.startTime);
  $scope.notification = notification;
  $scope.notification.startTime = $scope.notification.startTime ?
    calendar.timeToDate($scope.notification.startTime) :
    $scope.item.startTime;

  $scope.dateFormats = calendar.formats;

  $scope.submit = data => {
    $ionicLoading.show();
    data = _.clone(data);
    data.startTime = toHourFormat(data.startTime);
    return performAction(NotificationsService.set({ data, itemId: item.id }));
  };

  $scope.reset = () => {
    $ionicLoading.show();
    return performAction(NotificationsService.remove(item.id));
  };

  function performAction(method) {
    return method
    .then(() => $state.go('menu.program.join')
      .then(() => $state.reload('menu.program.join'))
    )
    .catch(response => {
      $ionicLoading.hide();
      showError(response.data.message || 'Error');
      return response;
    });
  }

  function toHourFormat(date) {
    return moment(date).format('HH:mm');
  }

  function showError(message) {
    $ionicPopup.alert({
      title: 'Error',
      template: message
    });
  }
});
