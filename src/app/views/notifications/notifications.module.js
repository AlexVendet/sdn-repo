angular
  .module('app.notifications', [])
  .config(function ($stateProvider) {

    $stateProvider
    .state('menu.program.notification', {
      url: '/item/:itemId/notification',
      templateUrl: 'views/notifications/edit/notifications-edit.html',
      controller: 'NotificationsEditController',
      params: {
        itemId: {
          value: '',
          squash: false
        }
      },
      resolve: {
        item: ['ProgramItemsService', '$stateParams', (ProgramItemsService, $stateParams) =>
          ProgramItemsService.get($stateParams.itemId).then(response => response.data)
        ],
        notification: ['NotificationsService', '$stateParams', '$q', (NotificationsService, $stateParams, $q) =>
          NotificationsService.get($stateParams.itemId).then(response => response.data)
          .catch(() => $q.resolve({}))
        ]
      }
    });
  });
