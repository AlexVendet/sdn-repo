angular
  .module('app.notifications')
  .service('NotificationsService', function ($timeout, $http, ServerUrls, $q) {
    const URL = ServerUrls.API_BASE + '/notification';
    const URLS = {
      SET: URL + '/set/',
      REMOVE: URL + '/delete/',
      GET_ALL: URL + '/getAll/',
      GET: URL + '/get/'
    };

    return {
      getAll,
      get,
      remove,
      set
    };

    function getAll(programId) {
      return $http.get(URLS.GET_ALL + programId);
    }

    function get(id) {
      return $http.get(URLS.GET + id);
    }

    function set({ itemId, data }) {
      return $http.post(URLS.SET + itemId, data, {});
    }

    function remove(id) {
      return $http.post(URLS.REMOVE + id);
    }
  });
