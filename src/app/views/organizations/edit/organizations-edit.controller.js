angular
.module('app.organizations')
.controller('OrganizationsEditController', function ($scope, $stateParams, $state, organization, $ionicPopup, OrganizationsService, $ionicHistory, $ionicLoading) {
  $scope.organization = organization;

  $scope.goBack = () => {
    if ($stateParams.organizationId === 'new' && $state.current.name === 'menu.organization.edit.step1') {
      return $ionicPopup.confirm({
        template: `Are you sure you want to cancel?`
      }).then(res => {
        if (res) {
          $ionicLoading.show();
          return OrganizationsService.remove($scope.organization.id)
            .finally(res => {
              $state.go('menu.organizations');
            });
        }
      });
    } else {
      $ionicHistory.goBack();
    }
  };
});
