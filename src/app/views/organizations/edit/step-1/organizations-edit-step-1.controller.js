angular
  .module('app.organizations')
  .controller('OrganizationsEditStepOneController',
  function ($scope, $stateParams, UploaderService, OrganizationsService, $ionicPopup, $state, $ionicLoading, programCategories, programTags) {
    const TAG_SEPARATOR = ', ';
    $scope.categories = programCategories.filter(item => !!item.code);
    $scope.organization = $scope.$parent.organization;
    $scope.forms = {};
    $scope.organizationId = $scope.organization.id;
    $scope.autoCompleteTags = [];

    $scope.tags = ($scope.organization.tags || '').split(TAG_SEPARATOR).filter(tag => !!tag).map(tag => ({ text: tag })) || [];

    $scope.onChange = () => {
      $scope.organization.tags = $scope.tags.map(tag => tag.text).join(TAG_SEPARATOR);
    };

    $scope.onInternalInputChange = (val='') => {
      console.log('val!?  ', val);
      $scope.autoCompleteTags = programTags
        .filter(tag => tag.toLowerCase().includes(val.toLowerCase()))
        .map(tag => ({ text: tag }));
      console.log('$scope.autoCompleteTags! ', $scope.autoCompleteTags);
    };
    $scope.uploadLogo = (options={}) => {
      let DEFAULT_OPTIONS = {
        cropRatio: 1
      };
      UploaderService.uploadDialog({ options: _.defaults(options, DEFAULT_OPTIONS), pathOptions: { organizationId: $scope.organizationId } })
        .then(data => {
          $scope.organization.logoFilename = data[0].amazonUrl;
          return data;
        });
    };

    let showError = function (message) {
      $ionicPopup.alert({
        title: 'Error',
        template: message
      });
    };

    $scope.remove = () =>
      $ionicPopup.confirm({
        template: `Are you sure you want to delete "${$scope.organization.name}" organization?`
      })
      .then(response => {
        if (response) {
          $ionicLoading.show();
          return OrganizationsService.remove($scope.organizationId)
            .then(() => {
              $state.go('menu.organizations');
            })
            .finally(() => $ionicLoading.hide())
            .catch(error => showError(error.data.message));
        }
      });

  });
