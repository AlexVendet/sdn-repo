angular
  .module('app.organizations')
  .controller('OrganizationsEditStepTwoController',
  function ($scope, OrganizationsService, $ionicLoading, $ionicPopup, $state, $stateParams, UploaderService, documents, $q, stateHelper) {
    $scope.organization = $scope.$parent.organization;

    $scope.documents = documents.map(doc => {
      doc.added = true;
      return doc;
    });
    console.log('$scope.documents! ', $scope.documents);
    let isNewOrganization = $stateParams.organizationId === 'new';

    let showError = function (message) {
      $ionicPopup.alert({
        title: 'Error',
        template: message
      });
    };

    $scope.uploadDocs = () => {
      const options = { multiple: true, mimetypes: ['*/*'] };
      return UploaderService.uploadDialog({ options, pathOptions: { organizationId: $scope.organization.id, customFolderName: 'documents' } })
        .then(response => {
          let data = response.map(item => ({
            filename: item.amazonUrl,
            name: item.filename,
            added: false
          }));
          $scope.documents = data.concat($scope.documents);
        });
    };

    $scope.deleteDoc = id => {
      $ionicLoading.show();
      return OrganizationsService.removeDocument({ id })
        .then(() => {
          stateHelper.reload();
        })
        .catch(response => {
          showError(response.data.message || 'Error');
        })
        .finally(() => $ionicLoading.hide());
    };

    function uploadDocument({ data, id }) {
      return OrganizationsService.addDocument({ data, id })
        .then(response => response.data);
    }

    $scope.submit = data => {
      $ionicLoading.show();
      let documentsToUpload = $scope.documents.filter(doc => doc.added === false)
        .map(doc => ({
          filename: doc.filename,
          name: doc.name,
          description: doc.description
        }));

      return OrganizationsService.update({ data, id: $scope.organization.id })
        .then(response =>
           $q.all(documentsToUpload.map(doc =>
             uploadDocument({ data: doc, id: response.data.id })
              .then(() => response)
          )))
          .then(response => {
            if (isNewOrganization) {
              $state.go('menu.organization.edit.step3', { organizationId: $scope.organization.id });
            } else {
              $state.go('menu.organizations');
            }
          })
          .catch(response => {
            $ionicLoading.hide();
            showError(response.data.message || 'Error');
          });
    };

    $scope.openDoc = url => {
      window.open(url, '_blank', 'location=yes');
    };
  });
