angular
  .module('app.organizations')
  .controller('OrganizationsEditStepThreeController', function ($scope, organization) {
    $scope.organization = organization;
  });
