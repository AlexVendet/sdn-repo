angular
  .module('app.organizations')
  .controller('OrganizationsListController', function ($scope, organizations) {
    $scope.organizations = organizations;
  });
