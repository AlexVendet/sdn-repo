angular
  .module('app.organizations', [])
  .config(function ($stateProvider) {

    $stateProvider

    .state('menu.organizations', {
      url: '/organizations',
      templateUrl: 'views/organizations/list/organizations-list.html',
      controller: 'OrganizationsListController',
      resolve: {
        organizations: ['OrganizationsService',
            OrganizationsService => OrganizationsService.getAll()
              .then(response => response.data)
          ]
      }
    })
    .state('menu.organization', {
      url: '/organization/:organizationId',
      abstract: true,
      template: '<ion-nav-view></ion-nav-view>',
      params: {
        organizationId: {
          value: '',
          squash: false
        }
      },
      resolve: {
        organization: ['OrganizationsService', '$stateParams', '$q', '$state', '$ionicPopup', (OrganizationsService, $stateParams, $q, $state, $ionicPopup) => {
          if ($stateParams.organizationId === 'new') {
            return OrganizationsService.create({})
              .then(response => response.data)
              .catch(err => {
                $ionicPopup.alert({
                  title: 'Error',
                  template: err.data.message || 'Unable to crate organization'
                });
                return $q.reject(err);
              });
          }
          return OrganizationsService.getAll()
            .then(response => {
              let organization = response.data.find(el => el.id === Number($stateParams.organizationId));
              return organization || $q.reject('No organization found');
            });
        }]
      }
    })
    .state('menu.organization.edit', {
      url: '',
      abstract: true,
      templateUrl: 'views/organizations/edit/organizations-edit.html',
      controller: 'OrganizationsEditController'
    })
    .state('menu.organization.edit.step1', {
      url: '',
      templateUrl: 'views/organizations/edit/step-1/organizations-edit-step-1.html',
      controller: 'OrganizationsEditStepOneController'
    })
    .state('menu.organization.edit.step2', {
      url: '/edit-2',
      templateUrl: 'views/organizations/edit/step-2/organizations-edit-step-2.html',
      controller: 'OrganizationsEditStepTwoController',
      resolve: {
        documents: ['OrganizationsService', '$stateParams', '$q', (OrganizationsService, $stateParams, $q) => {
          let emptyDocuments = [];
          if ($stateParams.organizationId === 'new') {
            return $q.resolve(emptyDocuments);
          }
          return OrganizationsService.getAllDocuments($stateParams.organizationId)
            .then(response => response.data)
            .catch(() => $q.resolve([]));
        }]
      }
    })
    .state('menu.organization.edit.step3', {
      url: '/edit-3',
      templateUrl: 'views/organizations/edit/step-3/organizations-edit-step-3.html',
      controller: 'OrganizationsEditStepThreeController',
      resolve: {
        organization: ['OrganizationsService', '$stateParams', '$q',
        (OrganizationsService, $stateParams, $q) =>
           OrganizationsService.getAll()
            .then(response => {
              let organization = response.data.find(el => el.id === Number($stateParams.organizationId));
              return organization || $q.reject('No organization found');
            })
        ]
      }
    });
  });
