angular
  .module('app.organizations')
  .service('OrganizationsService', function ($timeout, $http, ServerUrls, $q) {
    const URL = ServerUrls.API_BASE + '/organization';
    const URLS = {
      CREATE: URL + '/create',
      UPDATE: URL + '/update/',
      REMOVE: URL + '/delete/',
      GET_ALL: URL + '/getList',
      GET: URL + '/get/',
      SET_AS_DEFAULT: URL + '/setAsDefault',
      ADD_DOCUMENT: URL + '/addDocument/',
      REMOVE_DOCUMENT: URL + '/deleteDocument/',
      GET_ALL_DOCUMENTS: URL + '/getDocumentList/'
    };

    return {
      getAll,
      get,
      getDefault,
      create,
      update,
      remove,
      setAsDefault,
      addDocument,
      removeDocument,
      getAllDocuments
    };

    function getAll() {
      return $http.get(URLS.GET_ALL);
    }

    function get(id) {
      return $http.get(URLS.GET + id);
    }

    function getDefault() {
      return getAll()
        .then(response => {
          var defaultOrg = _.find(response.data, { isDefault: 1 });
          return defaultOrg ?
            $q.resolve(defaultOrg) :
            $q.reject('no default organization found');
        });
    }

    function create(data) {
      return $http.post(URLS.CREATE, data, {

      });
    }

    function update({ id, data }) {
      return $http.post(URLS.UPDATE + id, data, {

      });
    }

    function remove(id) {
      return $http.post(URLS.REMOVE + id);
    }

    function setAsDefault(id) {
      return $http.post(URLS.SET_AS_DEFAULT + id, null, {

      });
    }

    function addDocument({ id, data }) {
      return $http.post(URLS.ADD_DOCUMENT + id, data, {});
    }

    function removeDocument({ id }) {
      return $http.post(URLS.REMOVE_DOCUMENT + id);
    }

    function getAllDocuments(id) {
      return $http.get(URLS.GET_ALL_DOCUMENTS + id);
    }
  });
