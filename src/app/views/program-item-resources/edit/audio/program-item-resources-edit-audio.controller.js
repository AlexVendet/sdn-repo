angular
  .module('app.program.item.resources')
  .controller('ProgramItemResourcesEditAudioController', function ($scope, $stateParams, resource, PlatformService) {
    $scope.resource = resource;

    $scope.config = {
      type: 'AUDIO',
      uploadDevice: { services: ['COMPUTER', 'AUDIO'], mimetypes: ['audio/*'], openTo: PlatformService.isMobile() ? 'COMPUTER' : 'AUDIO' },
      uploadFolder: { services: ['COMPUTER', 'GOOGLE_DRIVE', 'DROPBOX', 'BOX', 'URL'], mimetypes: ['audio/*'] },
      uploadDeviceIcon: 'sdn-mic',
      uploadFolderIcon: 'sdn-folder'
    };
  });
