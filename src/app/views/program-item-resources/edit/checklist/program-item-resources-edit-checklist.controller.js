angular
  .module('app.program.item.resources')
  .controller('ProgramItemResourcesEditChecklistController', function ($scope, $stateParams, resource, $timeout, $element) {
    $scope.resource = resource;
    $scope.resource.checklistItems = _.sortBy($scope.resource.checklistItems || [{}], 'id');
    $scope.add = () => {
      $scope.resource.checklistItems.push({});
    };

    $scope.removeItem = item => {
      _.remove($scope.resource.checklistItems, item);
    };

    $scope.onEnter = $event => {
      $scope.add();
      $timeout(() => {
        _.last($element.find('input')).focus();
      }, 100);
    };
  });
