angular
.module('app.program.item.resources')
.controller('ProgramItemResourcesEditImageController', function ($scope, $stateParams, resource, ImagePreviewService) {
  $scope.resource = resource;
  $scope.url = resource.url;
  $scope.preview = () => {
    ImagePreviewService.open($scope);
  };
  $scope.config = {
    type: 'IMAGE',
    uploadDevice: { services: ['WEBCAM', 'CONVERT'], mimetypes: ['image/*'] },
    uploadFolder: { services: ['COMPUTER', 'CONVERT', 'FACEBOOK', 'GOOGLE_DRIVE', 'DROPBOX', 'BOX', 'IMAGE_SEARCH', 'URL'], mimetypes: ['image/*'] },
    uploadDeviceIcon: 'sdn-camera',
    uploadFolderIcon: 'sdn-folder'
  };
});
