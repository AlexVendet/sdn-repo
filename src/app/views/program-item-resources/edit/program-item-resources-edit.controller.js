angular.module('app.program.item.resources')

.controller('ProgramItemResourcesEditController', function ($scope, item, resource, $stateParams, $state, $ionicPopup, ProgramItemResourcesService, $ionicLoading, $ionicHistory, calendar) {
  $scope.resource = resource;

  $scope.item = item;
  $scope.isNew = $stateParams.resourceId === 'new';
  $scope.dateFormats = calendar.formats;

  $state.go('menu.organization.program.item.resource.edit.' + item.type.toLowerCase());

  $scope.remove = () => {
    $ionicLoading.show();
    return ProgramItemResourcesService.remove($scope.resource.id)
      .finally(() => $state.go('menu.organization.programs', { tab: 'upload' }));
  };

  let showError = function (message) {
    $ionicPopup.alert({
      title: 'Error',
      template: message
    });
  };

  let performSubmit = () => {
    if ($scope.isNew) {
      return ProgramItemResourcesService.create({ itemId: item.id, data: $scope.resource });
    } else {
      return ProgramItemResourcesService.update({ id: $scope.resource.id, data: $scope.resource });
    }
  };

  $scope.submit = () => {
    $ionicLoading.show();
    return performSubmit()
      .then(response => {
        $state.go('menu.organization.programs', { tab: 'upload' });
        return response.data;
      })
      .catch(response => {
        $ionicLoading.hide();
        showError(response.data.message || 'Error');
      });
  };

  $scope.openLink = url => {
    window.open(url, '_blank', 'menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,toolbar=no');
  };
}).filter('lodashNameFilter', function () {
  return val => _.last(val.split('_'));
});
