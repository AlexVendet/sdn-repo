angular
.module('app.program.item.resources')
.component('resourceEditUrl', {
  controller: 'ProgramItemResourcesEditUrlController',
  templateUrl: 'views/program-item-resources/edit/url/program-item-resources-edit.url.html',
  bindings: {
    config: '<',
    resource: '='
  }
})

.controller('ProgramItemResourcesEditUrlController', function ($stateParams, UploaderService, calendar, ImagePreviewService) {
  this.dateFormats = calendar.formats;
  this.upload = options =>
    UploaderService.uploadDialog({
      options,
      pathOptions: {
        organizationId: $stateParams.organizationId,
        programId: $stateParams.programId,
        itemId: $stateParams.itemId
      }
    })
    .then(data => {
      this.resource.url = data[0].amazonUrl;
    });

  this.resetUrl = () => {
    delete this.resource.url;
  };

  this.openLink = url => {
    if (this.config.type === 'IMAGE') {
      ImagePreviewService.open(url);
    } else {
      window.open(url, '_blank', 'menubar=no,location=no,resizable=no,scrollbars=no,status=no,toolbar=no');
    }
  };
});
