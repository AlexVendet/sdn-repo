angular
  .module('app.program.item.resources')
  .controller('ProgramItemResourcesEditVideoController', function ($scope, $stateParams, resource) {
    $scope.resource = resource;
    $scope.config = {
      type: 'VIDEO',
      uploadDevice: { services: ['VIDEO'], mimetypes: ['video/*'] },
      uploadFolder: { services: ['COMPUTER', 'GOOGLE_DRIVE', 'DROPBOX', 'BOX', 'URL'], mimetypes: ['video/*'] },
      uploadDeviceIcon: 'sdn-video',
      uploadFolderIcon: 'sdn-folder'
    };
  });

