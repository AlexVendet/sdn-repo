angular
  .module('app.program.item.resources')
  .controller('ProgramItemResourcesListCoachController', function ($scope, resources, item, calendar, ProgramItemResourcesService, $ionicPopup, $ionicLoading, stateHelper) {
    $scope.resources = resources;
    $scope.item = item;
    $scope.dateFormat = calendar.formats.DEFAULT;

    let showError = message => {
      $ionicPopup.alert({
        title: 'Error',
        template: message || 'Error'
      });
    };

    $scope.remove = id =>
      $ionicPopup.confirm({
        template: `Are you sure you want to delete the resource?`
      })
      .then(response => {
        if (response) {
          $ionicLoading.show();
          return ProgramItemResourcesService.remove(id)
            .then(() => stateHelper.reload())
            .finally(() => $ionicLoading.hide())
            .catch(error => showError(error.data.message));
        }
      });
  });
