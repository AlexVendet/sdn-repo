angular
  .module('app.program.item.resources')
  .controller('ProgramItemResourcesListSubscriberController', function ($scope, program, calendar, resources) {
    $scope.dateFormat = calendar.formats.DEFAULT;
    $scope.program = program;
    $scope.resources = resources;
  });
