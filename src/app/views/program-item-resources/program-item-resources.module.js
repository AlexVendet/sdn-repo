angular
.module('app.program.item.resources', [])
.config(function ($stateProvider) {
  $stateProvider
  .state('menu.organization.program.item.resources', {
    url: '/resources',
    templateUrl: 'views/program-item-resources/list/coach/program-item-resources-list-coach.html',
    controller: 'ProgramItemResourcesListCoachController',
    resolve: {
      resources: ['ProgramItemsService', '$stateParams', (ProgramItemsService, $stateParams) =>
        ProgramItemsService.getResources($stateParams.itemId)
          .then(response => response.data)
      ]
    }
  })
  .state('menu.organization.program.item.resource', {
    url: '/resource/:resourceId?publishDate',
    params: {
      resourceId: {
        value: '',
        squash: false
      },
      publishDate: {
        value: '',
        squash: true
      }
    },
    abstract: true,
    template: '<ion-nav-view></ion-nav-view>',
    resolve: {
      resource: ['ProgramItemResourcesService', '$stateParams', '$q', 'item', (ProgramItemResourcesService, $stateParams, $q, item) => {
        if ($stateParams.resourceId === 'new') {
          return $q.resolve({ publishDate: $stateParams.publishDate + (item.startTime ? `T${item.startTime}:00.000Z` : '') });
        }
        return ProgramItemResourcesService.get($stateParams.resourceId)
          .then(response => response.data);
      }]
    }
  })
  .state('menu.organization.program.item.resource.edit', {
    url: '/edit',
    templateUrl: 'views/program-item-resources/edit/program-item-resources-edit.html',
    controller: 'ProgramItemResourcesEditController'
  })
  .state('menu.organization.program.item.resource.edit.image', {
    url: '/image',
    templateUrl: 'views/program-item-resources/edit/image/program-item-resources-edit-image.html',
    controller: 'ProgramItemResourcesEditImageController'
  })
  .state('menu.organization.program.item.resource.edit.video', {
    url: '/video',
    templateUrl: 'views/program-item-resources/edit/video/program-item-resources-edit-video.html',
    controller: 'ProgramItemResourcesEditVideoController'
  })
  .state('menu.organization.program.item.resource.edit.audio', {
    url: '/audio',
    templateUrl: 'views/program-item-resources/edit/audio/program-item-resources-edit-audio.html',
    controller: 'ProgramItemResourcesEditAudioController'
  })
  .state('menu.organization.program.item.resource.edit.dnd', {
    url: '/dnd',
    templateUrl: 'views/program-item-resources/edit/dnd/program-item-resources-edit-dnd.html',
    controller: 'ProgramItemResourcesEditDndController'
  })
  .state('menu.organization.program.item.resource.edit.phrase', {
    url: '/phrase',
    templateUrl: 'views/program-item-resources/edit/phrase/program-item-resources-edit-phrase.html',
    controller: 'ProgramItemResourcesEditPhraseController'
  })
  .state('menu.organization.program.item.resource.edit.checklist', {
    url: '/checklist',
    templateUrl: 'views/program-item-resources/edit/checklist/program-item-resources-edit-checklist.html',
    controller: 'ProgramItemResourcesEditChecklistController'
  })

  .state('menu.organization.program.item.resource.view', {
    url: '/view',
    templateUrl: 'views/program-item-resources/view/program-item-resources-view.html',
    controller: 'ProgramItemResourcesViewController'
  })
  .state('menu.organization.program.item.resource.view.image', {
    url: '/image',
    templateUrl: 'views/program-item-resources/view/image/program-item-resources-view-image.html',
    controller: 'ProgramItemResourcesViewImageController'
  })
  .state('menu.organization.program.item.resource.view.video', {
    url: '/video',
    templateUrl: 'views/program-item-resources/view/video/program-item-resources-view-video.html',
    controller: 'ProgramItemResourcesViewVideoController'
  })
  .state('menu.organization.program.item.resource.view.audio', {
    url: '/audio',
    templateUrl: 'views/program-item-resources/view/audio/program-item-resources-view-audio.html',
    controller: 'ProgramItemResourcesViewAudioController'
  })
  .state('menu.organization.program.item.resource.view.dnd', {
    url: '/dnd',
    templateUrl: 'views/program-item-resources/view/dnd/program-item-resources-view-dnd.html',
    controller: 'ProgramItemResourcesViewDndController'
  })
  .state('menu.organization.program.item.resource.view.phrase', {
    url: '/phrase',
    templateUrl: 'views/program-item-resources/view/phrase/program-item-resources-view-phrase.html',
    controller: 'ProgramItemResourcesViewPhraseController'
  })
  .state('menu.organization.program.item.resource.view.checklist', {
    url: '/checklist',
    templateUrl: 'views/program-item-resources/view/checklist/program-item-resources-view-checklist.html',
    controller: 'ProgramItemResourcesViewChecklistController'
  })
  .state('menu.program.resources', {
    url: '/resources',
    templateUrl: 'views/program-item-resources/list/subscriber/program-item-resources-list-subscriber.html',
    controller: 'ProgramItemResourcesListSubscriberController',
    resolve: {
      resources: ['ProgramsService', 'ProgramItemsService', '$stateParams', '$q', (ProgramsService, ProgramItemsService, $stateParams, $q) => {
        const MAX_HOURS = 48;
        return ProgramsService.getAllItems($stateParams.programId)
          .then(response =>
            $q.all(response.data.map(item =>
              ProgramItemsService.getResources(item.id)
                .then(response => response.data
                  .filter(resource => {
                    let diff = Number(moment().diff(moment(resource.publishDate), 'hours'));
                    return diff < MAX_HOURS && diff > 0;
                  }).map(resource => {
                    resource.title = item.title;
                    resource.type = item.type;
                    resource.itemId = item.id;
                    return resource;
                  }))
            ))
            .then(response => _.concat(...response))
          );
      }]
    }
  })

  .state('menu.program.resource', {
    url: '/item/:itemId/resource/:resourceId/view',
    templateUrl: 'views/program-item-resources/view/program-item-resources-view.html',
    controller: 'ProgramItemResourcesViewController',
    params: {
      resourceId: {
        value: '',
        squash: false
      },
      itemId: {
        value: '',
        squash: false
      }
    },
    resolve: {
      resource: ['ProgramItemResourcesService', '$stateParams', (ProgramItemResourcesService, $stateParams) =>
        ProgramItemResourcesService.get($stateParams.resourceId).then(response => response.data)
      ],
      item: ['ProgramItemsService', '$stateParams', (ProgramItemsService, $stateParams) =>
        ProgramItemsService.get($stateParams.itemId).then(response => response.data)
      ]
    }
  })
  .state('menu.program.resource.image', {
    url: '/image',
    templateUrl: 'views/program-item-resources/view/image/program-item-resources-view-image.html',
    controller: 'ProgramItemResourcesViewImageController'
  })
  .state('menu.program.resource.video', {
    url: '/video',
    templateUrl: 'views/program-item-resources/view/video/program-item-resources-view-video.html',
    controller: 'ProgramItemResourcesViewVideoController'
  })
  .state('menu.program.resource.audio', {
    url: '/audio',
    templateUrl: 'views/program-item-resources/view/audio/program-item-resources-view-audio.html',
    controller: 'ProgramItemResourcesViewAudioController'
  })
  .state('menu.program.resource.dnd', {
    url: '/dnd',
    templateUrl: 'views/program-item-resources/view/dnd/program-item-resources-view-dnd.html',
    controller: 'ProgramItemResourcesViewDndController'
  })
  .state('menu.program.resource.phrase', {
    url: '/phrase',
    templateUrl: 'views/program-item-resources/view/phrase/program-item-resources-view-phrase.html',
    controller: 'ProgramItemResourcesViewPhraseController'
  })
  .state('menu.program.resource.checklist', {
    url: '/checklist',
    templateUrl: 'views/program-item-resources/view/checklist/program-item-resources-view-checklist.html',
    controller: 'ProgramItemResourcesViewChecklistController'
  });
});
