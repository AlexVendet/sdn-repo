angular
  .module('app.program.item.resources')
  .service('ProgramItemResourcesService', function ($timeout, $http, ServerUrls, $q) {
    const URL = ServerUrls.API_BASE + '/programItemResource';
    const URLS = {
      CREATE: URL + '/create/',
      UPDATE: URL + '/update/',
      REMOVE: URL + '/delete/',
      GET: URL + '/get/'
    };

    return {
      get,
      create,
      update,
      remove
    };

    function get(resourceId) {
      return $http.get(URLS.GET + resourceId);
    }

    function create({ data, itemId }) {
      return $http.post(URLS.CREATE + itemId, data, {});
    }

    function update({ id, data }) {
      return $http.post(URLS.UPDATE + id, data, {});
    }

    function remove(id) {
      return $http.post(URLS.REMOVE + id);
    }

  });
