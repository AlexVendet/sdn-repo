angular
  .module('app.program.item.resources')
  .controller('ProgramItemResourcesViewChecklistController', function ($scope, resource) {
    $scope.resource = resource;
    $scope.resource.checklistItems = _.sortBy($scope.resource.checklistItems || [{}], 'id');
  });
