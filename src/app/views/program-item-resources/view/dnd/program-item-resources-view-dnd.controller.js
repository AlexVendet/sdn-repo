angular
  .module('app.program.item.resources')
  .controller('ProgramItemResourcesViewDndController', function ($scope, $stateParams, resource) {
    $scope.resource = resource;
  });
