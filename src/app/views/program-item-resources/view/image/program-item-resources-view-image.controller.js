angular
.module('app.program.item.resources')
.controller('ProgramItemResourcesViewImageController', function ($scope, $stateParams, resource, ImagePreviewService) {
  $scope.resource = resource;

  $scope.preview = () => {
    ImagePreviewService.open(resource.url);
  };
});
