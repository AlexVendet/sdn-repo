angular
  .module('app.program.item.resources')
  .controller('ProgramItemResourcesViewPhraseController', function ($scope, $stateParams, resource) {
    $scope.resource = resource;
  });
