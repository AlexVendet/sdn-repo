angular.module('app.program.item.resources')

.controller('ProgramItemResourcesViewController', function ($scope, resource, item, calendar, $state) {
  const STATE_PREFIX = $state.current.name.startsWith('menu.program.resource') ?
    'menu.program.resource.' :
    'menu.organization.program.item.resource.view.';

  $scope.resource = resource;
  $scope.item = item;
  $scope.dateFormats = calendar.formats;
  $state.go(STATE_PREFIX + item.type.toLowerCase());

  $scope.openLink = url => {
    window.open(url, '_blank', 'menubar=no,location=no,resizable=no,scrollbars=no,status=no,toolbar=no');
  };
});
