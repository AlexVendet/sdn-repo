angular
  .module('app.program.items')
  .controller('DaySelectController', function (calendar) {
    this.weekDays =  calendar.weekDays;

    this.isSelected = day => _.includes(this.days, day);

    this.select = day => {
      let days = _.clone(this.days) || [];
      if (this.isSelected(day)) {
        _.pull(days, day);
      } else if (this.maxSelected === 1) {
        days = [day];
      } else if (!_.isUndefined(this.maxSelected) && this.maxSelected >= days.length) {
        return;
      } else {
        days.push(day);
      }
      this.setDay({ days });
    };

  })
  .component('daySelect', {
    templateUrl: 'views/program-items/edit/day-select/day-select.html',
    controller: 'DaySelectController',
    bindings: {
      days: '<',
      disabled: '<',
      setDay: '&',
      maxSelected: '<'
    }
  });
