angular
  .module('app.program.items')
  .controller('ProgramItemsEditController', function ($scope, $stateParams, item, $state, $ionicPopup, $ionicLoading, ProgramItemsService, program, calendar) {
    $scope.item = normalizeFormData(item);
    $scope.itemId = $stateParams.itemId;
    $scope.selection = 'phrase';
    $scope.program = program;
    let isNewProgram = $stateParams.itemId === 'new';

    $scope.types = [
      {
        value: 'PHRASE',
        name: 'Phrase'
      },
      {
        value: 'IMAGE',
        name: 'Image'
      },
      {
        value: 'VIDEO',
        name: 'Video'
      },
      {
        value: 'AUDIO',
        name: 'Audio'
      },
      {
        value: 'CHECKLIST',
        name: 'Checklist'
      },
      {
        value: 'DND',
        name: 'DND'
      }
    ];

    $scope.selectRecurringOption = ({ option }) => {
      $scope.item.recurringOption = option;
    };

    $scope.updateInputMode = isDynamic => {
      $scope.item.inputMode = isDynamic ? 'DYNAMIC' : 'STATIC';
    };

    $scope.submit = data => {
      $ionicLoading.show();
      return perfomSubmit(data)
        .then(response => {
          $state.go('menu.organization.program.items', { reload: true });
          return response.data;
        })
        .catch(response => {
          $ionicLoading.hide();
          showError(response.data.message || 'Error');
        });
    };

    function normalizeFormData(data) {
      data = _.clone(data);
      data.startTime = data.startTime ? calendar.timeToDate(data.startTime) : data.startTime;
      data.endTime = data.endTime ? calendar.timeToDate(data.endTime) : data.endTime;
      data.recurringOption = data.recurringOption || 'WEEKLY';
      return data;
    }

    function toHourFormat(date) {
      return moment(date).format('HH:mm');
    }

    function perfomSubmit(data) {
      data = _.clone(data);
      data.startTime = toHourFormat(data.startTime);
      data.endTime = toHourFormat(data.endTime);
      if (isNewProgram) {
        return ProgramItemsService.create({ data, programId: $stateParams.programId });
      } else {
        return ProgramItemsService.update({ data, id: data.id });
      }
    }

    $scope.setDay = ({ days }) => {
      $scope.item.recurringWeekDay  = days;
    };

    function showError(message) {
      $ionicPopup.alert({
        title: 'Error',
        template: message
      });
    }

  });
