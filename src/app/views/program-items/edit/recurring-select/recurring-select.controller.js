angular
  .module('app.program.items')
  .controller('recurringSelectController', function ($scope) {
    this.recurringOptions =  [
      { value: 'DAILY', name: 'Daily', disabled: true },
      { value: 'WEEKLY', name: 'Weekly', disabled: false },
      { value: 'MONTHLY', name: 'Monthly', disabled: true },
      { value: 'CUSTOM', name: 'Custom', disabled: true },
      { value: 'WEEKDAY', name: 'Weekday', disabled: true },
      { value: 'WEEKDEND', name: 'Weekdend', disabled: true }
    ];

  })
  .component('recurringSelect', {
    templateUrl: 'views/program-items/edit/recurring-select/recurring-select.html',
    controller: 'recurringSelectController',
    bindings: {
      recurringOption: '<',
      select: '&'
    }
  });
