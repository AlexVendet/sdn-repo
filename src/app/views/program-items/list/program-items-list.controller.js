angular
  .module('app.program.items')
  .controller('ProgramItemsListController', function ($scope, items, program) {
    $scope.items = _.sortBy(items, 'id');
    $scope.program = program;
  });
