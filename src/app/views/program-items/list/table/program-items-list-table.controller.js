angular
  .module('app.program.items')
  .controller('ProgramItemsListTableController', function ($scope) {
    const weekDays = {
      'MONDAY': { short: 'M', dayNum: 1 },
      'TUESDAY':  { short: 'T', dayNum: 2 },
      'WEDNESDAY':  { short: 'W', dayNum: 3 },
      'THURSDAY':  { short: 'T', dayNum: 4 },
      'FRIDAY':  { short: 'F', dayNum: 5 },
      'SATURDAY':  { short: 'S', dayNum: 6 },
      'SUNDAY':  { short: 'S', dayNum: 7 }
    };

    $scope.$ctrl.items = expandItems($scope.$ctrl.items);

    function expandItems(items) {
      return items.map(item => {
        item.recurringWeekDayShotcuts = makeWeekDaysShortcuts(item.recurringWeekDay);
        return item;
      });
    }

    function makeWeekDaysShortcuts(table) {
      let weekDaysShort = _.times(7, i => ' ');

      table.map(day => {
        let dayObj = weekDays[day];
        if(dayObj) {weekDaysShort[dayObj.dayNum-1] = dayObj.short;}
      });

      return weekDaysShort;
    }

    this.onClick = event => {
      if (this.disabled) {
        event.preventDefault();
      }
    };

  })
  .component('programItemsListTable', {
    templateUrl: 'views/program-items/list/table/program-items-list-table.html',
    controller: 'ProgramItemsListTableController',
    bindings: {
      items: '<',
      disabled: '<'
    }
  });
