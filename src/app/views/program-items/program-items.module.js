angular
.module('app.program.items', [])
.config(function ($stateProvider) {

  $stateProvider

  .state('menu.organization.program.items', {
    url: '/items',
    templateUrl: 'views/program-items/list/program-items-list.html',
    controller: 'ProgramItemsListController',
    resolve: {
      items: ['ProgramsService', '$stateParams', (ProgramsService, $stateParams) =>
        ProgramsService.getAllItems($stateParams.programId)
          .then(response => response.data)
        ]
    }
  })
  .state('menu.organization.program.item', {
    url: '/item/:itemId',
    params: {
      itemId: {
        value: '',
        squash: false
      }
    },
    abstract: true,
    template: '<ion-nav-view></ion-nav-view>',
    resolve: {
      item: ['ProgramItemsService', '$stateParams', '$q', (ProgramItemsService, $stateParams, $q) => {
        let newProgram = {};
        if ($stateParams.itemId === 'new') {
          return $q.resolve(newProgram);
        }
        return ProgramItemsService.get($stateParams.itemId)
          .then(response => response.data);
      }]
    }
  })
  .state('menu.organization.program.item.edit', {
    url: '',
    templateUrl: 'views/program-items/edit/program-items-edit.html',
    controller: 'ProgramItemsEditController'
  });
});
