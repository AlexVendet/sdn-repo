angular
  .module('app.program.items')
  .service('ProgramItemsService', function ($timeout, $http, ServerUrls) {
    const URL = ServerUrls.API_BASE + '/programItem';
    const URLS = {
      CREATE: URL + '/create/',
      UPDATE: URL + '/update/',
      REMOVE: URL + '/delete/',
      GET: URL + '/get/',
      GET_RESOURCES: URL + '/getItemResources/'
    };

    return {
      create,
      get,
      getResources,
      update,
      remove
    };

    function create({ data, programId }) {
      return $http.post(URLS.CREATE + programId, normalizeFormData(data), {});
    }

    function get(itemId) {
      return $http.get(URLS.GET + itemId);
    }

    function getResources(itemId) {
      return $http.get(URLS.GET_RESOURCES + itemId);
    }

    function update({ id, data }) {
      return $http.post(URLS.UPDATE + id, normalizeFormData(data), {});
    }

    function remove(id) {
      return $http.post(URLS.REMOVE + id);
    }

    function normalizeFormData(data) {
      data = _.clone(data);
      return data;
    }

    function toTimestamp(date) {
      return Number(moment(date).format('x'));
    }
  });
