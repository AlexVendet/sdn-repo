angular
  .module('app.programs')
  .constant('programTags', [
    'Productivity',
    'Time management',
    'Goal setting',
    'Get things done',
    'Personal Development',
    'Learn a foreign language',
    'Social Dynamics',
    'Health',
    'Fitness',
    'Focus',
    'Meditation',
    'Mindfulness',
    'High vibrating audios',
    'Gain Awareness',
    'Spirituality',
    'Motivation',
    'Time',
    'Alternative',
    'Happiness',
    'Personal',
    'Free',
    'Raise',
    'Creativity',
    'career'
  ]);
