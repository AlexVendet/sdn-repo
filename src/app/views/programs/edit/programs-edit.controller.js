angular
  .module('app.programs')
  .controller('ProgramsEditController', function ($scope, $stateParams, program, ProgramsService, $ionicHistory, $ionicPopup, $ionicLoading, $state) {
    $scope.program = program;

    $scope.goBack = () => {
      if ($stateParams.programId === 'new' && $state.current.name === 'menu.organization.program.edit.step1') {
        return $ionicPopup.confirm({
          template: `Are you sure you want to cancel?`
        }).then(res => {
          if (res) {
            $ionicLoading.show();
            return ProgramsService.remove($scope.program.id)
              .finally(() => {
                $state.go('menu.organization.programs', { tab: 'active' });
              });
          }
        });
      } else {
        $ionicHistory.goBack();
      }
    };
    $scope.$on('$destroy', $event => {
      /*
        New program is already created. Remove it if it hasn't been finished.
      */
      if (!$scope.program.name) {
        ProgramsService.remove($scope.program.id);
      }
    });
  });
