angular
  .module('app.programs')
  .controller('ProgramsEditStepOneController', function ($scope, $stateParams, UploaderService, ProgramsService, $ionicPopup, $state, $ionicLoading, programCategories, programTags) {
    const TAG_SEPARATOR = ', ';
    $scope.forms = {};
    $scope.categories = programCategories.filter(item => !!item.code);
    $scope.program = $scope.$parent.program;
    $scope.programId = $stateParams.programId;
    $scope.tags = ($scope.program.tags || '').split(TAG_SEPARATOR).filter(tag => !!tag).map(tag => ({ text: tag })) || [];

    $scope.program.languageCode = 'EN';

    $scope.onChange = () => {
      $scope.program.tags = $scope.tags.map(tag => tag.text).join(TAG_SEPARATOR);
    };

    $scope.onInternalInputChange = (val='') => {
      $scope.autoCompleteTags = programTags
        .filter(tag => tag.toLowerCase().includes(val.toLowerCase()))
        .map(tag => ({ text: tag }));
    };

    $scope.upload = (options={}, key='logoFilename') => {
      let DEFAULT_OPTIONS = {
        cropRatio: 1
      };
      return UploaderService.uploadDialog({
        options: _.defaults(options, DEFAULT_OPTIONS),
        pathOptions: {
          organizationId: $stateParams.organizationId,
          programId: $scope.program.id
        }
      })
      .then(data => {
        $scope.program[key] = data[0].amazonUrl;
        return data;
      });
    };

    let showError = function (message) {
      $ionicPopup.alert({
        title: 'Error',
        template: message
      });
    };

    $scope.updateLanguage = value => {
      if (!_.isUndefined(value)) {
        $scope.program.languageCode = value ? 'EN' : 'SP';
      }
    };

    $scope.remove = () =>
      $ionicPopup.confirm({
        template: `Are you sure you want to delete "${$scope.program.name || ''}" program?`
      }).then(response => {
        if (response) {
          $ionicLoading.show();
          return ProgramsService.unPublish({ id: $scope.program.id })
            .then(() => ProgramsService.remove($scope.program.id))
            .then(() => {
              $state.go('menu.organization.programs');
            })
            .finally(() => $ionicLoading.hide())
            .catch(error => showError(error.data.message));
        }
      });

  })
  .directive('bindInternalInputTo', function () {
    return {
      restrict: 'A',
      scope: false,
      link: function postLink(scope, element, attr) {
        let input = element.find('input');
        input.on('keyup', () => {
          scope.onInternalInputChange(input[0].value);
        });
      }
    };
  });
