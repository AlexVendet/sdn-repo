angular
  .module('app.programs')
  .controller('ProgramsEditStepTwoController',
  function ($scope, ProgramsService, $ionicLoading, $ionicPopup, $state, $stateParams, OrganizationsService) {
    $scope.program = $scope.$parent.program;
    let isNewProgram = $stateParams.programId === 'new';

    let perfomSubmit = data => {
      if (isNewProgram) {
        return OrganizationsService.getDefault()
          .then(response =>
            ProgramsService.create({ data, organizationId: response.id })
          );
      } else {
        return ProgramsService.update({ data, id: data.id });
      }
    };

    let showError = function (message) {
      $ionicPopup.alert({
        title: 'Error',
        template: message
      });
    };

    $scope.submit = data => {
      $ionicLoading.show();
      return perfomSubmit(data)
        .then(response => {
          $state.go('menu.organization.programs', { tab: response.data.status === 'PUBLISHED' ? 'active' : 'draft' }, { reload: true });
          return response.data;
        })
        .catch(response => {
          $ionicLoading.hide();
          showError(response.data.message || 'Error');
        });
    };

    $scope.openLink = url => {
      window.open(url, '_blank', 'location=yes');
    };
  });
