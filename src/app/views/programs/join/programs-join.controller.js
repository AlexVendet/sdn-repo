angular
  .module('app.programs')
  .controller('ProgramsJoinController',
  function ($scope, program, items, calendar, notifications) {
    $scope.program = program;
    $scope.weekDays = calendar.weekDays;

    $scope.hours = _.chain(items)
      .map(item => {
        let customNotification = _.find(notifications, { programItemId: item.id });
        if (customNotification) {
          item.startTime = customNotification.starTime || item.startTime;
          item.customNotification = true;
        }
        return item;
      })
      .groupBy(item => item.startTime.split(':')[0] || 0)
      .map((allItems, hour) => ({
        items: _.groupBy(allItems, item => item.recurringWeekDay[0] || 'MONDAY'),
        hour: Number(hour)
      }))
      .sortBy('hour')
      .value();
  });
