angular
  .module('app.programs')
  .controller('ProgramsListCoachController',
  function ($scope, programs, stateHelper, $stateParams, ProgramsService, $ionicLoading, $ionicPopup, $ionicListDelegate,
  $state, $ionicTabsDelegate, $timeout, $q, organization, calendar, ProgramItemsService, programStatus) {
    const TABS = {
      active: 0,
      draft: 1,
      upload: 2
    };
    setPrograms(programs);

    $timeout(()=> {
      setTab($stateParams.tab);
    });

    $scope.dateFormats = calendar.formats;

    $scope.organizationName = organization.name;
    $scope.weekFrom = moment($stateParams.date).isoWeekday(0);
    $scope.weekTo = moment($stateParams.date).isoWeekday(7);
    $scope.previousWeek = moment($stateParams.date).add(-7, 'day');
    $scope.nextWeek = moment($stateParams.date).add(7, 'day');

    function setPrograms(items) {
      items = _.sortBy(items, 'id');
      $scope.programs = items;
      getProgramItems($scope.programs);
      $scope.activePrograms = items.filter(item => item.status === programStatus.PUBLISHED);
      $scope.inactivePrograms = items.filter(item => item.status === programStatus.DRAFT || item.status === programStatus.FROZEN);
    }

    $scope.getResource = (item, publishDate) => {
      let formatted = publishDate.format(calendar.formats.DEFAULT);
      return item.resources.find(resource =>
        resource.publishDate.substr(0, formatted.length) === formatted);
    };

    function getProgramItems(programs) {
      if (!programs.length) {
        return;
      }
      $ionicLoading.show();
      return $q.all(programs.map(program =>
        ProgramsService.getAllItems(program.id)
          .then(response => {
            let resourcePromises = [];
            program.items = _.chain(response.data)
              .filter(item => item.type !== 'DND')
              .filter(item => item.inputMode !== 'STATIC')
              .map(item => {
                item.resources = [];
                resourcePromises.push(
                  ProgramItemsService.getResources(item.id)
                  .then(response => {
                    item.resources = response.data || item.resources;
                    return item;
                  })
                );
                return item;
              })
              .groupBy(item => _.capitalize(item.recurringWeekDay[0] || 'MONDAY'))
              .map((allItems, weekDay)=> {
                let date = moment($stateParams.date).isoWeekday(weekDay);
                return {
                  items: allItems,
                  order: date.isoWeekday(),
                  date,
                  weekDay
                };
              })
              .sortBy('order')
              .value();
            return $q.all(resourcePromises);
          }))
      )
      .finally(() => $ionicLoading.hide());
    }

    function setTab(tab) {
      $ionicTabsDelegate.$getByHandle('programsListCoach').select(TABS[tab] || 0);
    }

    $scope.updateState = params => {
      stateHelper.updateParams(params);
      setTab(params.tab);
    };

    $scope.timeToNumber = item => {
      return Number((item.startTime || '').replace(':', '.'));
    };

    function showError(message) {
      $ionicPopup.alert({
        title: 'Error',
        template: message
      });
    }

    $scope.setPublish = ({ id, publish = true }) => {
      $ionicLoading.show();
      let method = publish ? ProgramsService.publish : ProgramsService.freeze;
      return method({ id })
        .then(() => stateHelper.reload())
        .catch(response => {
          $ionicLoading.hide();
          showError(response.data.message || 'Error');
        })
        .finally(() => $ionicListDelegate.closeOptionButtons());
    };
  });
