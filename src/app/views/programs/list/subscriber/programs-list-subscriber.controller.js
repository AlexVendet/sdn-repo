angular
  .module('app.programs')
  .controller('ProgramsListSubscriberController',
  function ($scope, programs, stateHelper, $stateParams, ProgramsService, $ionicLoading, $ionicPopup,
  $ionicListDelegate, $state, $ionicTabsDelegate, $timeout, $q, SubscriptionsService, programStatus) {
    const TABS = {
      join: 0,
      active: 1,
      onHold: 2
    };

    setPrograms(programs);

    $timeout(()=> {
      setTab($stateParams.tab);
    });

    function setPrograms(items) {
      items = _.sortBy(items, 'id');
      $scope.programs = items.map(el => {
        el.recurringOption = el.recurringOption || ['MONDAY'];
        return el;
      });
      $scope.activePrograms = items.filter(item => item.status === programStatus.PUBLISHED && item.individualStatus !== programStatus.PAUSED);
      $scope.holdPrograms = items.filter(item => item.status !== programStatus.PUBLISHED || item.individualStatus === programStatus.PAUSED);
    }

    function setTab(tab) {
      $ionicTabsDelegate.$getByHandle('programsListSubscriber').select(TABS[tab] || 0);
    }

    $scope.updateState = params => {
      stateHelper.updateParams(params);
      setTab(params.tab);
    };

    function showError(message) {
      $ionicPopup.alert({
        title: 'Error',
        template: message
      });
    }

    function changeProgramState(promise) {
      return promise
        .then(() => stateHelper.reload())
        .catch(response => {
          $ionicLoading.hide();
          showError(response.data.message || 'Error');
        })
        .finally(() => $ionicListDelegate.closeOptionButtons());
    }

    $scope.unsubscribe = id => {
      $ionicLoading.show();
      return changeProgramState(SubscriptionsService.remove(id));
    };

    $scope.pause = id => {
      $ionicLoading.show();
      return changeProgramState(SubscriptionsService.pause(id));
    };

    $scope.subscribe = id => {
      $ionicLoading.show();
      return changeProgramState(SubscriptionsService.create({ id }));
    };
  });
