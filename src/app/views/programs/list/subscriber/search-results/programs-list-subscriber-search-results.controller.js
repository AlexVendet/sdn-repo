angular
  .module('app.programs')
  .controller('ProgramsListSearchResultsController', function ($scope, $stateParams, programStatus, programs, allPrograms) {
    const SEARCHABLE_PROGRAM_FIELDS = [
      'category', 'description', 'name', 'tags', 'languageCode'
    ];

    let getProgramValues = program =>
      SEARCHABLE_PROGRAM_FIELDS.map(field=> program[field] && program[field].toLowerCase ? program[field].toLowerCase() : program[field]);

    let filterByText = (program, query) => query ? _.find(getProgramValues(program), key => _.includes(key, query.toLowerCase())) : true;
    let filterByQuery = program => filterByText(program, $stateParams.query);

    let filterByCategoryKeywords = program => $stateParams.categoryKeywords ?
      _.find($stateParams.categoryKeywords.split(','), key => filterByText(program, key)) : true;

    let filterByExactCategory = program =>
      $stateParams.categoryCode ? program.category.toLowerCase() === $stateParams.categoryCode.toLowerCase() : true;

    let filterByCategory = program => $stateParams.categoryKeywords ? filterByCategoryKeywords(program) : filterByExactCategory(program);
    let filterByStatus = program => program.status === programStatus.PUBLISHED;
    let filterByAlreadyJoined = program => !_.find(programs, { id: program.id });

    $scope.results = allPrograms
      .filter(filterByStatus)
      .filter(filterByAlreadyJoined)
      .filter(filterByQuery)
      .filter(filterByCategory);

    $scope.resultsText = $stateParams.categoryName ? 'category ' + $stateParams.categoryName : 'phrase "' + $stateParams.query + '"';
  });
