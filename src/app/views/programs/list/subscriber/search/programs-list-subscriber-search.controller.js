angular
  .module('app.programs')
  .controller('ProgramsListSearchController', function ($scope, programs, programCategories) {
    $scope.programs = programs;
    $scope.categories = programCategories.filter(category => !category.disabled);
  });
