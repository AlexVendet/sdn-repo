'use strict';

angular.module('app.programs').constant('programCategories', [
  {
    name: 'Meditation & Mindfulness',
    code: 'MEDITATION_MINDFULNESS',
    keywords: ['Meditation', 'Mindfulness'],
    disabled: false,
    img: 'assets/images/meditation_mindfulness.jpg'
  },
  {
    name: 'Personal Development',
    code: 'PERSONAL_DEVELOPMENT',
    keywords: [],
    disabled: false,
    img: 'assets/images/personal_development.jpg'
  },
  {
    name: 'Health & Fitness',
    code: 'HEALTH_FITNESS',
    keywords: ['Health', 'Fitness'],
    disabled: false,
    img: 'assets/images/health_fitness.jpg'
  },
  {
    name: 'Wealth & Productivity',
    code: 'WEALTH_PRODUCTIVITY',
    keywords: ['wealth', 'productivity'],
    disabled: false,
    img: 'assets/images/wealth_productivity.jpg'
  },
  {
    name: 'Dating & Relationships',
    code: 'DATING_RELATIONSHIPS',
    keywords: ['Dating', 'Relationships'],
    disabled: false,
    img: 'assets/images/dating_relationship.jpg'
  },
  {
    name: 'Life Style',
    code: 'LIFESTYLE',
    keywords: ['Life Style', 'LifeStyle'],
    disabled: false,
    img: 'assets/images/lifestyle.jpg'
  },
  {
    name: 'Free & Promotionals',
    keywords: ['Free', 'Promo'],
    disabled: false,
    img: 'assets/images/free.jpg'
  },
  {
    name: 'En Español',
    keywords: ['SP', 'Español', 'Spanish'],
    disabled: false,
    img: 'assets/images/espaniol.jpg'
  }
]);
