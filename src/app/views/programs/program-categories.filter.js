angular
  .module('app.programs')
  .filter('programCategoriesFilter', function (programCategories) {
    return code => (_.find(programCategories, { code }) || {}).name || code;
  });
