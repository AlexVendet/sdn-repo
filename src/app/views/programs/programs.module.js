angular
.module('app.programs', [])
.config(function ($stateProvider) {

  $stateProvider

  .state('menu.organization.programs', {
    url: '/programs?tab&date',
    params: {
      tab: {
        value: 'upload',
        squash: false
      },
      date: {
        value: moment().format('YYYY-MM-DD'),
        squash: false
      }
    },
    templateUrl: 'views/programs/list/coach/programs-list-coach.html',
    controller: 'ProgramsListCoachController',
    resolve: {
      programs: ['ProgramsService', '$stateParams', '$q', 'OrganizationsService', '$state',
      (ProgramsService, $stateParams, $q, OrganizationsService, $state) => {
        if (!$stateParams.organizationId) {
          return OrganizationsService.getDefault()
            .then(organization => {
              $state.go('menu.organization.programs', { organizationId: organization.id }, { reload: true });
              return ProgramsService.getAllForOrganization(organization.id)
                .then(response => response.data);
            })
            .catch(error => {
              $state.go('menu.organizations');
            });
        }
        return ProgramsService.getAllForOrganization($stateParams.organizationId)
          .then(response => response.data);
      }]
    }
  })
  .state('menu.organization.program', {
    url: '/program/:programId',
    abstract: true,
    params: {
      programId: {
        value: '',
        squash: false
      }
    },
    resolve: {
      program: ['ProgramsService', '$stateParams', '$q', '$ionicPopup', (ProgramsService, $stateParams, $q, $ionicPopup) => {
        if ($stateParams.programId === 'new') {
          return ProgramsService.create({ data: {}, organizationId: $stateParams.organizationId })
            .then(response => response.data)
            .catch(err => {
              $ionicPopup.alert({
                title: 'Error',
                template: err.data.message || 'Unable to crate program'
              });
              return $q.reject(err);
            });
        }
        return ProgramsService.get(Number($stateParams.programId))
        .then(response => response.data);
      }]
    },
    template: '<ion-nav-view></ion-nav-view>'
  })
  .state('menu.organization.program.edit', {
    url: '',
    abstract: true,
    templateUrl: 'views/programs/edit/programs-edit.html',
    controller: 'ProgramsEditController'
  })
  .state('menu.organization.program.edit.step1', {
    url: '',
    templateUrl: 'views/programs/edit/step-1/programs-edit-step-1.html',
    controller: 'ProgramsEditStepOneController'
  })
  .state('menu.organization.program.edit.step2', {
    url: '/step-2',
    templateUrl: 'views/programs/edit/step-2/programs-edit-step-2.html',
    controller: 'ProgramsEditStepTwoController'
  })
  .state('menu.programs', {
    url: '/programs?tab',
    abstract: true,
    params: {
      tab: {
        value: 'join',
        squash: false
      }
    },
    templateUrl: 'views/programs/list/subscriber/programs-list-subscriber.html',
    controller: 'ProgramsListSubscriberController',
    resolve: {
      programs: ['ProgramsService', (ProgramsService) =>
        ProgramsService.getAllForSubscriber()
          .then(response => response.data)
      ]
    }
  })
  .state('menu.programs.search', {
    url: '',
    templateUrl: 'views/programs/list/subscriber/search/programs-list-subscriber-search.html',
    controller: 'ProgramsListSearchController'
  })
  .state('menu.programs.searchResults', {
    url: '/searchResults?categoryName&categoryCode&query&categoryKeywords',
    params: {
      categoryName: {
        value: '',
        squash: true
      },
      categoryCode: {
        value: '',
        squash: true
      },
      query: {
        value: '',
        squash: true
      },
      categoryKeywords: {
        value: '',
        squash: true
      }
    },
    templateUrl: 'views/programs/list/subscriber/search-results/programs-list-subscriber-search-results.html',
    controller: 'ProgramsListSearchResultsController',
    resolve: {
      allPrograms: ['ProgramsService', (ProgramsService) =>
        ProgramsService.getAll()
          .then(response => response.data)
      ]
    }
  })
  .state('menu.program', {
    url: '/program/:programId',
    abstract: true,
    params: {
      programId: {
        value: '',
        squash: false
      }
    },
    resolve: {
      program: ['ProgramsService', '$stateParams', (ProgramsService, $stateParams) =>
        ProgramsService.get(Number($stateParams.programId))
          .then(response => response.data)
      ],
      items: ['ProgramsService', '$stateParams', (ProgramsService, $stateParams) =>
        ProgramsService.getAllItems($stateParams.programId)
          .then(response => response.data)
      ]
    },
    template: '<ion-nav-view></ion-nav-view>'
  })
  .state('menu.program.view', {
    url: '',
    templateUrl: 'views/programs/view/programs-view.html',
    controller: 'ProgramsViewController'
  })
  .state('menu.program.join', {
    url: '/join',
    templateUrl: 'views/programs/join/programs-join.html',
    controller: 'ProgramsJoinController',
    resolve: {
      notifications: ['NotificationsService', '$stateParams', (NotificationsService, $stateParams) =>
        NotificationsService.getAll($stateParams.programId)
          .then(response => response.data)
      ]
    }
  });

});
