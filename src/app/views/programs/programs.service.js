angular
  .module('app.programs')
  .service('ProgramsService', function ($http, ServerUrls) {
    const URL = ServerUrls.API_BASE + '/program';
    const URLS = {
      CREATE: URL + '/create/',
      UPDATE: URL + '/update/',
      REMOVE: URL + '/delete/',
      GET: URL + '/get/',
      GET_ALL: URL + '/getList',
      GET_ALL_ITEMS: URL + '/getItems/',
      PUBLISH: URL + '/publish/',
      UNPUBLISH: URL + '/unpublish/',
      FREEZE: URL + '/freeze/'
    };

    return {
      get,
      getAll,
      getAllForOrganization,
      getAllForSubscriber,
      create,
      update,
      remove,
      getAllItems,
      publish,
      unPublish,
      freeze
    };

    function get(id) {
      return $http.get(URLS.GET + id);
    }

    function getAllForOrganization(organizationId) {
      return $http.get(URLS.GET_ALL + `/${organizationId}`);
    }

    function getAllForSubscriber() {
      return $http.get(URLS.GET_ALL);
    }

    function getAll() {
      return $http.get(URLS.GET_ALL + '/all');
    }

    function create({ data, organizationId }) {
      data.inputMode = data.type === 'DND' ? 'STATIC' : 'DYNAMIC';
      return $http.post(URLS.CREATE + organizationId, data, {});
    }

    function update({ id, data }) {
      data.inputMode = data.type === 'DND' ? 'STATIC' : 'DYNAMIC';
      return $http.post(URLS.UPDATE + id, data, {});
    }

    function remove(id) {
      return $http.post(URLS.REMOVE + id);
    }

    function getAllItems(id) {
      return $http.get(URLS.GET_ALL_ITEMS + id);
    }

    function publish({ id, timestamp = moment().format('x') }) {
      return $http.post(URLS.PUBLISH + `${id}/${timestamp}`);
    }

    function unPublish({ id }) {
      return $http.post(URLS.UNPUBLISH + id);
    }

    function freeze({ id }) {
      return $http.post(URLS.FREEZE + id);
    }
  });
