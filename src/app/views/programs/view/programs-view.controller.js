angular
  .module('app.programs')
  .controller('ProgramsViewController',
  function ($scope, program, items, OrganizationsService, $ionicPopup, $state, $stateParams, SubscriptionsService, $ionicLoading) {
    $scope.program = program;
    $scope.items = items;

    $scope.subscribe = () => {
      $ionicLoading.show();
      return SubscriptionsService.create({ id: program.id })
        .then(() => {
          $state.go('menu.program.join');
        })
        .finally(() => $ionicLoading.hide())
        .catch(error => showError(error.data.message));
    };

    function showError(message) {
      $ionicPopup.alert({
        title: 'Error',
        template: message || 'Error'
      });
    }
  });
