
angular.module('app.signUp', [])

.controller('SignUpController', function ($scope, $location, AuthenticationService, $ionicLoading, $ionicPopup, UserService) {
  $scope.isSignUp = false;

  $scope.signUp = function (data={}) {
    showLoadingScreen();
    return UserService.create(data)
      .then(response => {
        $ionicPopup.alert({
          title: 'You have successfully registered',
          subTitle: 'Please confirm your email address to sing in'
        });
        hideLoadingScreen();
      })
      .catch(response => {
        hideLoadingScreen();
        showError(response.data.error_description || 'Error');
      });
  };

  $scope.signIn = function (data={}) {
    showLoadingScreen();
    return AuthenticationService.signIn(data)
      .then(response => {
        $location.path('/');
        return response;
      })
      .catch(response => {
        hideLoadingScreen();
        showError(response.data.error_description || 'Error');
      });
  };

  function showLoadingScreen() {
    return $ionicLoading.show({
      content: 'Loading...',
      showBackdrop: false
    });
  }

  function hideLoadingScreen() {
    return $ionicLoading.hide();
  }

  function showError(message) {
    $ionicPopup.alert({
      title: 'Error',
      template: message
    });
  }
});
