angular.module('app.userProfile', [])

.controller('UserProfileController',
function ($scope, $location, userData, UserService, $ionicLoading, $stateParams) {
  $scope.user = _.clone(userData);

  $scope.showWelcomeHeader = $stateParams.welcome === true;
  $scope.submit = function (data) {
    $ionicLoading.show({
      content: 'Loading...',
      showBackdrop: false
    });
    return UserService.update({ id: data.id, data })
      .then(() => {
        // will be redirect to default page
        $location.path('/');
      })
      .finally(() => $ionicLoading.hide());
  };
});
